terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.13.0"
    }
  }
}

locals {
  ami      = "ami-061c10a2cb32f3491" // AmazonECSOptimized
}

module "runner" {
  count               = var.scale_units
  source              = "git::https://gitlab.com/mesabg-tfmodules/instance.git?ref=v1.0.0"

  environment         = var.environment
  name                = "${var.name}_runner_${count.index}"

  ami                 = local.ami
  instance_type       = var.instance_type
  key_name            = var.key_name
  security_groups     = var.security_groups
  subnet_id           = var.subnet_id
  associate_public_ip = true
  monitoring          = true
  volume_size         = 30

  user_data           = <<COMANDS
  #!/bin/bash
  yum update -y
  yum install vim -y
  yum upgrade -y
  docker version

  docker run --rm --name gitlab -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
    --url https://gitlab.com/ \
    --registration-token ${var.registration_token} \
    --executor docker \
    --docker-image ubuntu:20.04 \
    --docker-privileged \
    --env "DOCKER_TLS_CERTDIR=" \
    --tag-list "${var.instance_type}" \
    --run-untagged=true \
    --locked=false \
    --access-level=not_protected \
    --non-interactive

  docker run -d --name gitlab-runner --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner
  COMANDS
}
