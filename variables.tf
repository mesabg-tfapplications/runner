variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "Instance name"
}

variable "instance_type" {
  type        = string
  description = "Type of instance for the runner"
  default     = "t3a.small"
}

variable "scale_units" {
  type        = number
  description = "Amount of units to scale"
  default     = 4
}

variable "registration_token" {
  type        = string
  description = "Gitlab Registration Token"
}

variable "key_name" {
  type        = string
  description = "Key Pair name"
  default     = null
}

variable "security_groups" {
  type        = list(string)
  description = "Security group IDs"
}

variable "subnet_id" {
  type        = string
  description = "Subnet Association ID"
}
