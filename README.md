# Runner

This module is capable to deploy scalable gitlab runners on EC2 instances

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `instance_type` - (optional) type of instance (t3a.small is default)
- `scale_units` - (optional) amount of runners to launch (4 is default)
- `registration_token` - gitlab registration token
- `key_name` - (optional) valid key pair
- `security_groups` - security groups list
- `subnet_id` - subnet id

Usage
-----

```hcl
module "runner" {
  source              = "git::https://gitlab.com/mesabg-tfapplications/runner.git"

  environment         = "environment"

  name                = "instance name"

  scale_units         = 4
  registration_token  = "sometoken"
  key_name            = "my_keypair"
  security_groups     = ["sg-xxxxx"]
  subnet_id           = "subnet-xxxx"
}
```

```hcl
module "runner" {
  source              = "git::https://gitlab.com/mesabg-tfapplications/runner.git"

  environment         = "environment"

  name                = "instance name"

  instance_type       = "m2.medium"
  scale_units         = 15
  registration_token  = "sometoken"
  key_name            = "my_keypair"
  security_groups     = ["sg-xxxxx"]
  subnet_id           = "subnet-xxxx"
}
```

Outputs
=======

 - `instances` - Created instances resources


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
